# All U Need Is Pizza

> _Made With **NodeJS** 12_

Isomorphic Projects For Testing Technologies With CRUD Pattern

## File Structure

```
.
+-- public
    +-- favicon.png
    +-- index.html
+-- src
    +-- assets
        +-- fonts
            +-- Nunito-Bold.ttf
            +-- Nunito-Regular.ttf
    +-- components
        +-- containers
            +-- Add.jsx
            +-- index.js
            +-- Info.jsx
            +-- List.jsx
        +-- layers
            +-- __tests__
                +-- Block.spec.js
                +-- HyperLink.spec.js
                +-- Radio.spec.js
                +-- Row.spec.js
                +-- TextField.spec.js
            +-- Block.jsx
            +-- HyperLink.jsx
            +-- index.js
            +-- Radio.jsx
            +-- Row.jsx
            +-- TextField.jsx
        +-- App.jsx
        +-- Routing.jsx
    +-- redux
        +-- pizzas
            +-- __tests__
                +-- pizzas.spec.js
            +-- actions.js
            +-- constants.js
            +-- index.js
            +-- reducer.js
            +-- selectors.js
        +-- store.js
    +-- services
        +-- __tests__
            +-- pizza.spec.js
        +-- pizza.js
    +-- utils
        +-- index.js
    +-- index.css
    +-- index.js
    +-- pizzas.json
    +-- setupTests.js
+-- .browserslistrc
+-- .eslintrc.js
+-- .gitignore
+-- .prettierrc
+-- LICENSE
+-- package.json
+-- README.md
+-- yarn.lock
```

## Process

Repository:

```
git clone https://gitlab.com/aunip/state-management/react.git
```

Install:

```
npm install
```

Launch:

```
npm run start
```

Test:

```
npm run test
```

Build:

```
npm run build
```

## License

```
"THE BEER-WARE LICENSE" (Revision 42):
<phk@FreeBSD.ORG> wrote this file. As long as you retain this notice you
can do whatever you want with this stuff. If we meet some day, and you think
this stuff is worth it, you can buy me a beer in return. Damien Chazoule
```
