import * as Actions from '../actions';
import * as Constants from '../constants';
import pizzas from '../reducer';
import * as Selectors from '../selectors';

describe('Pizzas', () => {
  describe('Actions', () => {
    it('Should "setPizzas" Action Returns Right "type" & "payload"', () => {
      const payload = [
        {
          id: 'uguj2keowpq',
          label: '4 Fromages',
          items: ['Mozzarella', 'Chèvre', 'Reblochon', 'Gorgonzola'],
          price: 14.9
        }
      ];

      expect(Actions.setPizzas(payload)).toEqual({
        type: Constants.SET_PIZZAS,
        payload
      });
    });

    it('Should "resetPizzas" Action Returns Right "type" & "payload"', () => {
      expect(Actions.resetPizzas()).toEqual({
        type: Constants.RESET_PIZZAS
      });
    });

    it('Should "addPizza" Action Returns Right "type" & "payload"', () => {
      const payload = {
        label: '4 Fromages',
        items: ['Mozzarella', 'Chèvre', 'Reblochon', 'Gorgonzola'],
        price: 14.9
      };

      expect(Actions.addPizza(payload)).toEqual({
        type: Constants.ADD_PIZZA,
        payload
      });
    });

    it('Should "setPizza" Action Returns Right "type" & "payload"', () => {
      const payload = {
        id: 'uguj2keowpq',
        label: '4 Fromages',
        items: ['Mozzarella', 'Chèvre', 'Reblochon', 'Gorgonzola'],
        price: 14.9
      };

      expect(Actions.setPizza(payload)).toEqual({
        type: Constants.SET_PIZZA,
        payload
      });
    });

    it('Should "delPizza" Action Returns Right "type" & "payload"', () => {
      expect(Actions.delPizza('uguj2keowpq')).toEqual({
        type: Constants.DEL_PIZZA,
        payload: 'uguj2keowpq'
      });
    });
  });

  describe('Reducer', () => {
    const initialState = [];

    it('Should Default Case Returns Right State', () => {
      expect(pizzas()).toEqual(initialState);
    });

    it('Should "SET_PIZZAS" Case Returns Right State', () => {
      const payload = [
        {
          id: 'uguj2keowpq',
          label: '4 Fromages',
          items: ['Mozzarella', 'Chèvre', 'Reblochon', 'Gorgonzola'],
          price: 14.9
        }
      ];

      expect(pizzas(undefined, Actions.setPizzas(payload))).toEqual(payload);
    });

    it('Should "RESET_PIZZAS" Case Returns Right State', () => {
      const state = [
        {
          id: 'uguj2keowpq',
          label: '4 Fromages',
          items: ['Mozzarella', 'Chèvre', 'Reblochon', 'Gorgonzola'],
          price: 14.9
        }
      ];

      expect(pizzas(state, Actions.resetPizzas())).toEqual(initialState);
    });

    it('Should "ADD_PIZZA" Case Returns Right State', () => {
      const payload = {
        label: '4 Fromages',
        items: ['Mozzarella', 'Chèvre', 'Reblochon', 'Gorgonzola'],
        price: 14.9
      };

      expect(pizzas(undefined, Actions.addPizza(payload))).toHaveLength(initialState.length + 1);
    });

    it('Should "SET_PIZZA" Case Returns Right State', () => {
      const state = [
        {
          id: 'uguj2keowpq',
          label: '4 Fromages',
          items: ['Mozzarella', 'Chèvre', 'Reblochon', 'Gorgonzola'],
          price: 14.9
        },
        {
          id: 'u91diii1em',
          label: 'Calzone',
          items: ['Mozzarella', 'Jambon Blanc', 'Champignons', 'Emmental', 'Oeuf', 'Origan'],
          price: 13.9
        }
      ];

      const payload = {
        id: 'uguj2keowpq',
        label: '3 Fromages',
        items: ['Mozzarella', 'Gouda', 'Reblochon', 'Gorgonzola'],
        price: 13.9
      };

      expect(pizzas(state, Actions.setPizza(payload))).toEqual(state.map(value => (value.id === payload.id ? payload : value)));
    });

    it('Should "DEL_PIZZA" Case Returns Right State', () => {
      const state = [
        {
          id: 'uguj2keowpq',
          label: '4 Fromages',
          items: ['Mozzarella', 'Chèvre', 'Reblochon', 'Gorgonzola'],
          price: 14.9
        }
      ];

      expect(pizzas(state, Actions.delPizza('uguj2keowpq'))).toEqual(initialState);
    });
  });

  describe('Selectors', () => {
    const state = {
      pizzas: [
        {
          id: 'uguj2keowpq',
          label: '4 Fromages',
          items: ['Mozzarella', 'Chèvre', 'Reblochon', 'Gorgonzola'],
          price: 14.9
        }
      ]
    };

    it('getPizzas', () => {
      expect(Selectors.getPizzas({})).toHaveLength(0);
      expect(Selectors.getPizzas(state)).toHaveLength(1);
    });

    it('getPizzaById', () => {
      expect(Selectors.getPizzaById({})).toBeNull();
      expect(Selectors.getPizzaById(state, 'uguj2keowpq')).toEqual({
        id: 'uguj2keowpq',
        label: '4 Fromages',
        items: ['Mozzarella', 'Chèvre', 'Reblochon', 'Gorgonzola'],
        price: 14.9
      });
    });
  });
});
