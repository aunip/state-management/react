export const getPizzas = state => state.pizzas || [];

export const getPizzaById = (state, id) => {
  const pizzas = getPizzas(state);

  if (pizzas.length > 0) {
    return pizzas.find(pizza => pizza.id === id);
  }

  return null;
};
