import { SET_PIZZAS, DEL_PIZZA, RESET_PIZZAS, SET_PIZZA, ADD_PIZZA } from './constants';

/**
 * Set Pizzas
 *
 * @param {Array} pizzas Pizzas
 */
export const setPizzas = pizzas => ({ type: SET_PIZZAS, payload: pizzas });

/**
 * Reset Pizza
 */
export const resetPizzas = () => ({ type: RESET_PIZZAS });

/**
 * Add Pizza
 *
 * @param {Object} pizza Pizza
 */
export const addPizza = pizza => ({ type: ADD_PIZZA, payload: pizza });

/**
 * Set Pizza
 *
 * @param {Object} pizza Pizza
 */
export const setPizza = pizza => ({ type: SET_PIZZA, payload: pizza });

/**
 * Del Pizza
 *
 * @param {String} id ID
 */
export const delPizza = id => ({ type: DEL_PIZZA, payload: id });
