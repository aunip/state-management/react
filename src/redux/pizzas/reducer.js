import { SET_PIZZAS, RESET_PIZZAS, ADD_PIZZA, SET_PIZZA, DEL_PIZZA } from './constants';
import { generateId } from '../../utils';

const initialState = [];

export default function pizzas(state = initialState, action = {}) {
  const { type, payload } = action;

  switch (type) {
    case SET_PIZZAS:
      return payload;

    case RESET_PIZZAS:
      return initialState;

    case ADD_PIZZA:
      return [
        ...state,
        {
          id: generateId(),
          ...payload
        }
      ];

    case SET_PIZZA:
      return state.map(pizza => (pizza.id === payload.id ? payload : pizza));

    case DEL_PIZZA:
      return state.filter(({ id }) => id !== payload);

    default:
      return state;
  }
}
