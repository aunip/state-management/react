export const SET_PIZZAS = 'SET_PIZZAS';
export const RESET_PIZZAS = 'RESET_PIZZAS';
export const ADD_PIZZA = 'ADD_PIZZA';
export const SET_PIZZA = 'SET_PIZZA';
export const DEL_PIZZA = 'DEL_PIZZA';
