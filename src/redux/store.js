import { combineReducers, createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import pizzas from './pizzas';

const STORE = 'AUNIP';

const rootReducer = combineReducers({
  pizzas
});

const lastState = sessionStorage.getItem(STORE) ? JSON.parse(sessionStorage.getItem(STORE)) : {};

export const makeStore = (state = lastState) => {
  const middlewares = composeWithDevTools(applyMiddleware(thunk));

  return createStore(rootReducer, state, middlewares);
};

const store = makeStore();

store.subscribe(() => {
  sessionStorage.setItem(STORE, JSON.stringify(store.getState()));
});

export default store;
