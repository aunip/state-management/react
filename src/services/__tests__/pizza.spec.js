import { readAllPizzas } from '../pizza';

describe('Pizza Service', () => {
  it('readAllPizzas', async () => {
    const pizzas = await readAllPizzas();

    expect(pizzas).toHaveLength(25);
  });
});
