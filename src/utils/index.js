/**
 * Generate ID
 */
export const generateId = () => {
  return Math.random()
    .toString(36)
    .substring(2);
};

/**
 * toUpperCase() ShortCut
 *
 * @param {String} text Text
 * @returns {String} Formatted 'TEXT'
 */
export const up = text => text.toUpperCase();

/**
 * toLowerCase() ShortCut
 *
 * @param {String} text Text
 * @returns {String} Formatted 'text'
 */
export const lo = text => text.toLowerCase();

/**
 * Capitalize
 *
 * @param {String} text Text
 * @returns {String} Formatted 'Text'
 */
export const capitalize = text => up(text.substring(0, 1)) + lo(text.substring(1, text.length));

/**
 * Scroll To ListView
 */
export const scrollToListView = () => {
  const listView = document.querySelector('.listview');
  listView.scrollTo({
    top: 40,
    left: 0,
    behavior: 'smooth'
  });
};
