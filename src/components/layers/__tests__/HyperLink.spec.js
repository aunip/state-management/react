import React from 'react';
import { render, fireEvent, cleanup } from '@testing-library/react';
import HyperLink from '../HyperLink';

describe('<HyperLink />', () => {
  afterEach(cleanup);

  it('Should Component Renders', () => {
    const { container } = render(<HyperLink />);

    expect(container).toBeDefined();
  });

  it('Should Component Renders Simply', () => {
    const { getByText } = render(<HyperLink>Click Me</HyperLink>);

    expect(getByText('Click Me')).toBeInTheDocument();
  });

  it('Should Component Renders Fully', () => {
    const { container } = render(
      <HyperLink to="#test" size={18}>
        Click Me
      </HyperLink>
    );

    const hyperlink = container.querySelector('a');

    expect(hyperlink).toHaveAttribute('href', '#test');

    expect(hyperlink).toHaveStyle('font-size: 18px');
    // expect(hyperlink.style['font-size']).toEqual('18px');
  });

  it('Should Click Event Works Well', () => {
    const handleClick = jest.fn();

    const { getByText } = render(<HyperLink handleClick={handleClick}>Click Me</HyperLink>);

    fireEvent.click(getByText('Click Me'));

    expect(handleClick).toHaveBeenCalled();
  });
});
