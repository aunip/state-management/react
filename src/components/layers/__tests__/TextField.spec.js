import React from 'react';
import { render, fireEvent, cleanup } from '@testing-library/react';
import TextField from '../TextField';

describe('<TextField />', () => {
  afterEach(cleanup);

  it('Should Component Renders', () => {
    const { container } = render(<TextField />);

    expect(container).toBeDefined();
  });

  it('Should Component Renders Simply', () => {
    const { container, getByText } = render(
      <TextField>
        <span>Hello World</span>
      </TextField>
    );

    expect(container.querySelector('p')).toHaveStyle('font-size: 16px');
    // expect(container.querySelector('p').style['font-size']).toEqual('16px');

    expect(getByText('Hello World')).toBeInTheDocument();
  });

  it('Should Component Renders Fully', () => {
    const { getByPlaceholderText } = render(<TextField type="number" placeholder="Test" value={42} size={18} editable />);

    expect(getByPlaceholderText('Test')).toHaveAttribute('type', 'number');
    expect(getByPlaceholderText('Test')).toHaveStyle('font-size: 18px');
    expect(getByPlaceholderText('Test').value).toEqual('42');
  });

  it('Should Change Event Works Well', () => {
    const handleChange = jest.fn();

    const { getByPlaceholderText } = render(<TextField placeholder="Test" handleChange={handleChange} editable />);

    fireEvent.change(getByPlaceholderText('Test'), { target: { value: 'Hello World' } });

    expect(getByPlaceholderText('Test').value).toEqual('Hello World');

    expect(handleChange).toHaveBeenCalled();
  });
});
