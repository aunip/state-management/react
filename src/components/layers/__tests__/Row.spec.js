import React from 'react';
import { render, cleanup } from '@testing-library/react';
import Row from '../Row';

describe('<Row />', () => {
  afterEach(cleanup);

  it('Should Component Renders', () => {
    const { container } = render(<Row />);

    expect(container).toBeDefined();
  });

  it('Should Component Renders Simply', () => {
    const { container } = render(<Row />);

    expect(container.querySelector('.icon')).toBeInTheDocument();
  });

  it('Should Component Renders Fully', () => {
    const { getByText } = render(<Row leftCell={<span>Left</span>} rightCell={<span>Right</span>} />);

    expect(getByText('Left')).toBeInTheDocument();
    expect(getByText('Right')).toBeInTheDocument();
  });
});
