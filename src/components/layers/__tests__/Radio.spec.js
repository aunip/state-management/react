import React from 'react';
import { render, fireEvent, cleanup } from '@testing-library/react';
import Radio from '../Radio';

describe('<Radio />', () => {
  afterEach(cleanup);

  it('Should Component Renders', () => {
    const { container } = render(<Radio />);

    expect(container).toBeDefined();
  });

  it('Should Component Renders Fully', () => {
    const { container } = render(<Radio color="#fafafa" />);

    const icon = container.querySelector('svg');
    const circles = container.querySelectorAll('circle');

    expect(icon).toHaveStyle('cursor: pointer');
    // expect(icon.style['cursor']).toEqual('pointer');

    expect(circles).toHaveLength(2);
  });

  it('Should Click Event Works Well', () => {
    const handleClick = jest.fn();

    const { container } = render(<Radio color="#fafafa" handleClick={handleClick} />);

    const icon = container.querySelector('svg');

    // First Click
    fireEvent.click(icon);
    expect(container.querySelectorAll('circle')).toHaveLength(1);

    // Second Click
    fireEvent.click(icon);
    expect(container.querySelectorAll('circle')).toHaveLength(2);

    expect(handleClick).toHaveBeenCalledTimes(2);
  });
});
