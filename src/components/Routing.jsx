import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { List, Info, Add } from './containers';

function Routing() {
  return (
    <Switch>
      <Route exact path="/" component={List} />
      <Route path="/pizza/:id" render={props => <Info {...props} />} />
      <Route exact path="/new" component={Add} />
    </Switch>
  );
}

export default Routing;
