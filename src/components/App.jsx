import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import Routing from './Routing';
import { setPizzas } from '../redux/pizzas';
import { readAllPizzas } from '../services/pizza';

function App() {
  const dispatch = useDispatch();

  useEffect(() => {
    readAllPizzas().then(result => {
      dispatch(setPizzas(result));
    });
  });

  return <Routing />;
}

export default App;
