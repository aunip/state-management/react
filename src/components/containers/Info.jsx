import React, { useState } from 'react';
import { useParams, useHistory } from 'react-router';
import { useSelector, useDispatch } from 'react-redux';
import { Block, Row, HyperLink, TextField, Radio } from '../layers';
import { getPizzaById, delPizza, setPizza } from '../../redux/pizzas';
import { capitalize } from '../../utils';

function useLocked(initialState) {
  const [value, setValue] = useState(initialState);

  return [value, () => setValue(!value)];
}

function useInput(initialState) {
  const [value, setValue] = useState(initialState);

  return [value, event => setValue(event.target.value)];
}

function useArray(initialState) {
  const [values, setValues] = useState(initialState);

  return [values, index => event => setValues(values.map((value, idx) => (idx === index ? event.target.value : value)))];
}

function Info() {
  const { id } = useParams();
  const history = useHistory();
  const pizza = useSelector(state => getPizzaById(state, id));
  const dispatch = useDispatch();

  const [takeOff, setTakeOff] = useState([]);
  const [locked, setLocked] = useLocked(true);
  const [label, setLabel] = useInput(pizza.label);
  const [items, setItem] = useArray(pizza.items);
  const [price, setPrice] = useInput(pizza.price);

  const handleTakeOff = item => {
    if (takeOff.find(i => i === item)) {
      setTakeOff(takeOff.filter(i => i !== item));
    } else {
      setTakeOff([...takeOff, item]);
    }
  };

  const removePizza = () => {
    dispatch(delPizza(id));
    history.push('/');
  };

  const updatePizza = () => {
    if (!locked) {
      dispatch(
        setPizza({
          id,
          label: capitalize(label),
          items,
          price: parseFloat(price)
        })
      );
    }

    setLocked();
  };

  return (
    <div id="app">
      <Block color="#ef5350">
        <TextField value={label} placeholder="Label" handleChange={setLabel} size={18} editable={!locked}>
          {label}
        </TextField>
      </Block>
      <Block height={285} color="#ffca28">
        <div className="listview">
          {items.length > 0 &&
            items.map((item, idx) => (
              <Row
                key={idx}
                iconCell={<Radio handleClick={() => handleTakeOff(item)} />}
                leftCell={
                  <TextField value={item} placeholder="Item" handleChange={setItem(idx)} editable={!locked}>
                    {item}
                  </TextField>
                }
              />
            ))}
          <Row
            rightCell={
              <TextField type="number" value={price} placeholder="Price" handleChange={setPrice} editable={!locked}>
                {(price - takeOff.length).toFixed(2)} €
              </TextField>
            }
          />
        </div>
      </Block>
      <Block color="#ffa726">
        <HyperLink handleClick={updatePizza} size={18}>
          {locked ? 'Unlock' : 'Lock'}
        </HyperLink>
        <HyperLink handleClick={() => history.push('/')} size={18}>
          Back
        </HyperLink>
        {!locked && (
          <HyperLink handleClick={removePizza} size={18}>
            Del
          </HyperLink>
        )}
      </Block>
    </div>
  );
}

export default Info;
