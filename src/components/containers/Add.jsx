import React, { useState } from 'react';
import { useHistory } from 'react-router';
import { useDispatch } from 'react-redux';
import { Block, Row, HyperLink, TextField, Radio } from '../layers';
import { addPizza } from '../../redux/pizzas';
import { capitalize } from '../../utils';

function useInput(initialState) {
  const [value, setValue] = useState(initialState);

  return [value, event => setValue(event.target.value)];
}

function Add() {
  const history = useHistory();
  const dispatch = useDispatch();

  const [label, setLabel] = useInput('');
  const [items, setItems] = useState(['']);
  const [price, setPrice] = useInput(0);

  const addItem = index => event => {
    const value = event.target.value;

    const newItems = items.map((item, idx) => (idx === index ? value : item));

    if (value.length > 0) {
      if (items[index + 1] === undefined) {
        setItems([...newItems, '']);
      } else {
        setItems(newItems);
      }
    } else {
      setItems(newItems.filter((_, idx) => idx !== index + 1));
    }
  };

  const isValid = () => {
    const allItems = items.filter(item => item.length > 0);

    return label.length > 0 && allItems.length > 0 && price.length > 0;
  };

  const addNewPizza = () => {
    if (isValid()) {
      dispatch(
        addPizza({
          label: capitalize(label),
          items: items.filter(item => item.length > 0),
          price: parseFloat(price)
        })
      );

      history.push('/');
    }
  };

  return (
    <div id="app">
      <Block color="#ef5350">
        <TextField placeholder="Label" value={label} handleChange={setLabel} size={18} editable />
      </Block>
      <Block height={285} color="#ffca28">
        <div className="listview">
          {items.map((item, idx) => (
            <Row
              key={idx}
              iconCell={<Radio color={item.length > 0 ? '#212121' : '#fafafa'} />}
              leftCell={<TextField placeholder="Item" value={item} handleChange={addItem(idx)} editable />}
            />
          ))}
          <Row rightCell={<TextField type="number" placeholder="Price" value={price} handleChange={setPrice} editable />} />
        </div>
      </Block>
      <Block color="#ffa726">
        {isValid() && (
          <HyperLink handleClick={addNewPizza} size={18}>
            Save
          </HyperLink>
        )}
        <HyperLink handleClick={() => history.push('/')} size={18}>
          Back
        </HyperLink>
      </Block>
    </div>
  );
}

export default Add;
