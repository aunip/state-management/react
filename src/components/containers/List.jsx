import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router';
import { useSelector } from 'react-redux';
import { Block, Row, HyperLink, TextField } from '../layers';
import { getPizzas } from '../../redux/pizzas';
import { lo, scrollToListView } from '../../utils';

function useInput(initialState) {
  const [value, setValue] = useState(initialState);

  return [value, event => setValue(event.target.value)];
}

function List() {
  const history = useHistory();
  const pizzas = useSelector(state => getPizzas(state));

  const [filter, setFilter] = useInput('');

  useEffect(() => {
    setTimeout(() => {
      scrollToListView();
    }, 1000);
  }, []);

  const byLabel = ({ label }) => lo(label).includes(lo(filter));

  const byKey = key => (a, b) => (a[key] > b[key] ? 1 : a[key] < b[key] ? -1 : 0);

  return (
    <div id="app">
      <Block color="#ef5350">
        <TextField placeholder="All U Need Is Pizza" handleChange={setFilter} size={18} editable />
      </Block>
      <Block height={285} color="#ffca28">
        <div className="listview">
          <Row leftCell={<HyperLink handleClick={() => history.push('/new')}>New</HyperLink>} />
          {pizzas.length > 0 &&
            pizzas
              .filter(byLabel)
              .sort(byKey('label'))
              .map(({ id, label, price }) => (
                <Row
                  key={id}
                  leftCell={<HyperLink handleClick={() => history.push(`/pizza/${id}`)}>{label}</HyperLink>}
                  rightCell={<TextField>{price.toFixed(2)} €</TextField>}
                />
              ))}
        </div>
      </Block>
      <Block color="#ffa726">
        <TextField size={18}>
          {pizzas.filter(byLabel).length} / {pizzas.length}
        </TextField>
      </Block>
    </div>
  );
}

export default List;
